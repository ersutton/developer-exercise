# ABX Developer Exercise

## Instructions

1. Fork the repository.
2. Create a script or application that generates outputs to the following from data.csv:

   2a. The country with the highest average "Urban population growth (annual %)" between 1980 and 1990. Exclude countries where any data entry for this time range is missing.

   2b. The year with the highest "CO2 emissions (kt)", averaged across each country for which data is available.

3. Display the results to the user, however you choose to do so.
4. Create a pull request with your solution.

Note: There is no right or wrong way to achieve this. Please provide instructions on how to run your solution. Please use Docker where appropriate for access to dependencies (Databases, runtimes etc).

If you'd prefer not have this repo public on your Github, feel free to clone it into a private repo with your provider of choice. Give sam.jeston@abx.com view permissions when you are complete.

## Eds Instructions

- Please read the comments in /src/2b - it explains my 'workings'
- I have only written one unit test for one function - I would write more, but I have a weekend :)
- I have written this with zero dependencies: - I thought that would be the best way to demonstrate coding ability - I would normally see if there are libraries that exist first!
- I have steered away from using partial, pipe from functional libraries as I don't know how 'functional' you guys go...
- I have commited the env_local.env file for ease of use - I would never normally commit environment variables to version control!
- I have put the env variables in the start script as well - again, I would not normally do this!

### To run locally

`npm install && npm run buildAndStartEnv`

### To run in a docker container locally

`npm install && npm run buildAndRunDockerFile`

### To run in a docker container built using gitlabCI:

`npm run runDockerFileBuiltFromGitlabCI`
