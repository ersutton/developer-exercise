import { IRow, processCSVToIRow } from './countryDataReader';
import { calculate2a } from './2a';
import { calculate2bb, calculate2bc } from './2b';
import { cliBuilder } from './utilities';
import { isUndefined } from 'util';

/**
 * Please read the comments in 2b
 * I have only written one unit test for one function - I would write more, but I have a weekend :)
 * I have written this with zero dependencies:
 *    * I thought that would be the best way to demonstrate coding ability
 *    * I would normally see if there are libraries that exist first!
 * I have steered away from using partial, pipe from functional libraries as I don't know how 'functional' you guys go...
 * I have commited the env_local.env file for ease of use - I would never normally commit environment variables to version control!
 * I have put the env variables in the start script as well - again, I would not normally do this!
 *
 * To run locally:
 * npm install && npm run buildAndStartEnv
 *
 * To run in a docker container locally:
 * npm install && npm run buildAndRunDockerFile
 *
 * To run in a docker container built using gitlabCI:
 * npm run runDockerFileBuiltFromGitlabCI
 *
 *
 */

const startNumber = Number(process.env['START_ROW']);
const fileName = process.env['FILE_NAME'];

if (isNaN(startNumber)) {
  console.log('START_ROW environment variable needs to be set\nExiting...');
  process.exit();
}
if (isUndefined(fileName)) {
  console.log('FILE_NAME environment variable needs to be set\nExiting...');
  process.exit();
}

processCSVToIRow(startNumber, fileName)
  .then((entireData: IRow[]) => {
    function partiallyApplied2A(): void {
      return calculate2a(entireData);
    }
    function partiallyApplied2BB() {
      return calculate2bb(entireData);
    }
    function partiallyApplied2BC() {
      return calculate2bc(entireData);
    }
    return cliBuilder(
      'Please select a query: \n2a\n2bb\n2bc\n\nPress q to exit.',
      'failed to understand your input, please try again',
      { answer: '2a', functionForAnswer: partiallyApplied2A },
      { answer: '2bb', functionForAnswer: partiallyApplied2BB },
      { answer: '2bc', functionForAnswer: partiallyApplied2BC }
    );
  })
  .catch((err: Error) => {
    console.log('something went wrong!');
    console.error(err);
    process.exit();
  });
