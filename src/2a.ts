import {
  IRow,
  ICountryAverage,
  averageDataWithinDateRangeDescending,
} from './countryDataReader';

/**
 * 2a. The country with the highest average "Urban population growth (annual %)" between 1980 and 1990.
 *     Exclude countries where any data entry for this time range is missing.
 */

export function getCountriesByAverageUrbanPopulationGrowthDecsending(
  rows: IRow[]
): ICountryAverage[] {
  return averageDataWithinDateRangeDescending(rows, 'SP.URB.GROW', 1980, 1990);
}

export function calculate2a(rows: IRow[]) {
  const _2a: ICountryAverage[] = getCountriesByAverageUrbanPopulationGrowthDecsending(
    rows
  );
  if (_2a.length > 0) {
    console.log(
      `2a) The country with the highest average "Urban population growth (annual %)" between 1980 and 1990. is ${
        _2a[0].countryName
      } with an average population growth year on year of ${
        _2a[0].average
      }%.\nCountries and their average population growth year on year are listed in descending order:\n ${JSON.stringify(
        _2a,
        undefined,
        4
      )}`
    );
  } else {
    console.log(`2a) No Urban population growth data found!`);
  }
}
