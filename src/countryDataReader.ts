import {
  parseCSVLine,
  calculatePercentageTo2DP,
  roundToTwoDp,
  processFileLineByLine,
} from './utilities';
import { isUndefined } from 'util';

export interface ICountryAverage {
  countryName: string;
  average: number;
}
export interface IData {
  year: number;
  data: number;
}
export interface IRow {
  indicatorName: string;
  indicatorCode: string;
  countryName: string;
  countryCode: string;
  yearData: IData[];
}

export interface IYearWithLargestTotal {
  year: number;
  total: number;
  countriesPercentageOfTotal: ICountryAverage[];
}

// this is lazy and inefficient...
export const years: number[] = parseCSVLine(
  '"1960","1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017"'
).map((year: string) => Number(year));

/**
 * filters the data pulled from the csv to only have data on
 * a particular indicator code. For example, you could filter
 * the entire data set to only have data on "Urban Population Growth"
 * by passing in 'SP.URB.GROW' as the second parameter
 *
 * @export
 * @param {IRow[]} rows
 * @param {string} indicatorCode
 * @returns {IRow[]}
 */
export function getDataSetByIndicatorCode(
  rows: IRow[],
  indicatorCode: string
): IRow[] {
  return rows.filter((row: IRow) => row.indicatorCode === indicatorCode);
}

/**
 * takes an IRow (which is a line read from the .csv)
 * and looks at the yearly data, and removes any that
 * are not within the supplied range, returns a new IRow
 *
 * @export
 * @param {number} startYear
 * @param {number} endYear
 * @param {IRow} row
 * @returns {IRow}
 */
export function filterYearDataByDateRange(
  startYear: number,
  endYear: number,
  row: IRow
): IRow {
  const data: IData[] = row.yearData.filter(
    (yData: IData) => yData.year >= startYear && yData.year <= endYear
  );
  return { ...row, yearData: data };
}

/**
 * takes an IRow (which is a line read from the .csv) and
 * determines whether any of the annual data is NaN
 * if any yearly data is NaN it will return false
 *
 * use this in a .filter function
 *
 * @export
 * @param {IRow} row
 * @returns {boolean}
 */
export function filterRowIfSomeDataMissing(row: IRow): boolean {
  return !row.yearData.some((data: IData) => isNaN(data.year));
}

/**
 * gets the data for the current IRow year
 * returns NaN if it failed to find data for that year
 *
 * @export
 * @param {IRow} row
 * @param {number} year
 * @returns {(number)}
 */
export function getThisYearsData(row: IRow, year: number): number {
  const yearData: IData = row.yearData.find(
    (data: IData) => data.year === year
  );
  // coerce unfound data to be NaN for ease of filtering with years that really don't have data
  return isUndefined(yearData) ? NaN : yearData.data;
}

/**
 * itterate over each row and see if any of the row's data is missing.
 * returns years that have complete data for all countries
 *
 * @export
 * @param {IRow[]} row
 */
export function getYearsWithCompleteData(rows: IRow[]): number[] {
  const yearsWithMissingData: Set<number> = new Set();
  rows.forEach((row: IRow) => {
    row.yearData
      .filter((iData: IData) => isNaN(iData.data))
      .forEach((data: IData) => {
        yearsWithMissingData.add(data.year);
      });
  });

  return years.filter((year: number) => !yearsWithMissingData.has(year));
}
/**
 * calculates the total data for each country for each year specified
 * calculates the percentage of that total that each country contributed
 * returns this in descending order
 *
 *
 * @export
 * @param {number[]} years
 * @param {IRow[]} rows
 * @param {string} indicatorCode
 * @returns {IYearWithLargestTotal[]}
 */
export function totalForIndicatorCodeForEachYearWithPercentageCountryContributionAscending(
  years: number[],
  rows: IRow[],
  indicatorCode: string
): IYearWithLargestTotal[] {
  const dataSet: IRow[] = getDataSetByIndicatorCode(rows, indicatorCode);

  return (
    years
      .map((year: number) => {
        const valuesForThisYear: number[] = dataSet
          .map((row: IRow) => getThisYearsData(row, year))
          .filter((data: number) => !isNaN(data)); // remove any NaN's

        const total: number =
          valuesForThisYear.length === 0
            ? 0 // there was no data for this year for any country
            : // caluclate total
              valuesForThisYear.reduce(
                (previous, current) => (current += previous),
                0
              );

        const countriesPercentageOfTotal: ICountryAverage[] =
          total === 0
            ? [] // theres no data, so no contributing countries to that data
            : dataSet
                .map((row: IRow) => {
                  const thisYearsData: number = getThisYearsData(row, year);
                  const percentage: number = isNaN(thisYearsData)
                    ? 0 // no data for this year for this country? it contributed 0% then
                    : calculatePercentageTo2DP(total, thisYearsData);
                  return {
                    average: percentage,
                    countryName: row.countryName,
                  };
                })
                .sort((a, b) => b.average - a.average);

        return {
          year,
          total,
          countriesPercentageOfTotal,
        };
      })
      // TODO: take sort out, create function for ascending/descending
      .sort((a, b) => {
        return b.total - a.total;
      })
  );
}

/**
 * function that calcuates the total data in a date range and then finds the average
 * returns this value against the country that had the values
 * the values are returned in descending order
 *
 * @export
 * @param {IRow[]} rows
 * @param {string} indicatorCode
 * @param {number} startYear
 * @param {number} endYear
 * @returns {ICountryAverage[]}
 */
export function averageDataWithinDateRangeDescending(
  rows: IRow[],
  indicatorCode: string,
  startYear: number,
  endYear: number
): ICountryAverage[] {
  return getDataSetByIndicatorCode(rows, indicatorCode)
    .map((row: IRow) => filterYearDataByDateRange(startYear, endYear, row))
    .filter(filterRowIfSomeDataMissing)
    .map((row: IRow) => {
      const total: number = row.yearData
        .map((data: IData) => data.data)
        .reduce((previous, current) => (current += previous), 0);
      const averageToTwoDP: number = roundToTwoDp(total / row.yearData.length);
      return {
        countryName: row.countryName,
        average: averageToTwoDP,
      };
    })
    .sort((a, b) => b.average - a.average);
}

/**
 * the function that is called for each line read from the csv
 * it turns the data into a js object for ease of use
 *
 * @export
 * @param {string[]} row
 * @returns {IRow}
 */
export function convertLineToIRow(row: string[]): IRow {
  const clonedRow: string[] = [...row];
  return {
    countryName: clonedRow[0],
    countryCode: clonedRow[1],
    indicatorName: clonedRow[2],
    indicatorCode: clonedRow[3],
    yearData: clonedRow.slice(4).map((value: string, index: number) => {
      return {
        year: years[index],
        data: value === '' ? NaN : Number(value), // so empty strings and values that fail to parse both come out as NaN
      };
    }),
  };
}
/**
 * reads the passed in csv file and returns an array of IRow[]
 *
 * @export
 * @param {number} startLine
 * @param {string} fileName
 * @returns {Promise<IRow[]>}
 */
export function processCSVToIRow(
  startLine: number,
  fileName: string
): Promise<IRow[]> {
  function rowProcessor(row: string): IRow {
    return convertLineToIRow(parseCSVLine(row));
  }
  return processFileLineByLine<IRow>(startLine, fileName, rowProcessor);
}
