import {
  IRow,
  getDataSetByIndicatorCode,
  getYearsWithCompleteData,
  years,
  IYearWithLargestTotal,
  totalForIndicatorCodeForEachYearWithPercentageCountryContributionAscending as totalForIndicatorCodeForEachYearWithPercentageCountryContributionDescending,
} from './countryDataReader';

/**
 * "The year with the highest "CO2 emissions (kt)", averaged across each country for which data is available.""
 * does that mean:
 * a) filter to countries that have complete data sets across all years and then calculate total to find biggest c02 year.
 *    then 'averaged accoss each country' (to what? find which country had the biggest % of emissions?)
 * b) filter to years for which all countries have data for and then calcuate total to find biggest c02 year.
 *    then 'averaged across each country' (to what? find which country had the biggest % of emissions?)
 * c) total each years data (to find the highest c02 emitted), and then find the biggest value irrespective of whether a data set is complete, then calculate % of emissions for each country.
 *
 * I'm going to do b) (because that's what I originally thought) - but that might fail due to there not being any years with all countries having data.
 * I'm also doing c) because I think that might be actually what you are asking for.
 *
 * What I would say is, if I was working there - I'd ask, not guess! :)
 */

/**
 *
 *
 * first filter to the correct data type
 * then find any years that have data for all countries
 * then go through each year and collate total C02,
 * then calculate each countries percentage contribution,
 * then sort descending
 *
 *
 * @export
 * @param {IRow[]} rows
 * @returns {ICountryAverage[]}
 */
export function getYearsThatHaveC02DataForAllCountriesCalculateTotalDescending(
  rows: IRow[]
): IYearWithLargestTotal[] {
  const dataSetForC02: IRow[] = getDataSetByIndicatorCode(
    rows,
    'EN.ATM.CO2E.KT'
  );

  const yearsWithCompleteData: number[] = getYearsWithCompleteData(
    dataSetForC02
  );
  if (yearsWithCompleteData.length === 0) {
    // not a single year exists that has data for all countries
    return [];
  }
  // years exist with data for all countries
  return totalForIndicatorCodeForEachYearWithPercentageCountryContributionDescending(
    yearsWithCompleteData,
    rows,
    'EN.ATM.CO2E.KT'
  );
}

/**
 * returns in descending order the year with the largest c02 emisssions, and the countries
 * contribution to that
 *
 * @export
 * @param {IRow[]} rows
 * @returns {IYearWithLargestTotal[]}
 */
export function calculateTotalC02EmissionsWithCountriesContributionToTotalDescending(
  rows: IRow[]
): IYearWithLargestTotal[] {
  return totalForIndicatorCodeForEachYearWithPercentageCountryContributionDescending(
    years,
    rows,
    'EN.ATM.CO2E.KT'
  );
}

/**
 * please read explanation at top of this file
 *
 * @export
 * @param {IRow[]} rows
 */
export function calculate2bb(rows: IRow[]) {
  const _2b: IYearWithLargestTotal[] = getYearsThatHaveC02DataForAllCountriesCalculateTotalDescending(
    rows
  );
  if (_2b.length > 0) {
    console.log(
      `2bb) The year with the highest "CO2 emissions (kt)", averaged across each country for which data is available is ${
        _2b[0].year
      } with a total C02 emissions of ${
        _2b[0].total
      }.\nOffending countries are listed in descending order:\n ${JSON.stringify(
        _2b[0].countriesPercentageOfTotal,
        undefined,
        4
      )}`
    );
  } else {
    console.log(
      `2bb) Not a single year exists that has data for all countries`
    );
  }
}

/**
 * please read explanation at top of this file
 *
 * @export
 * @param {IRow[]} rows
 */
export function calculate2bc(rows: IRow[]) {
  const _2b: IYearWithLargestTotal[] = calculateTotalC02EmissionsWithCountriesContributionToTotalDescending(
    rows
  );
  if (_2b.length > 0) {
    console.log(
      `2bc) The year with the highest "CO2 emissions (kt)", averaged across each country for which data is available is ${
        _2b[0].year
      } with a total C02 emissions of ${
        _2b[0].total
      }.\nOffending countries are listed in descending order:\n ${JSON.stringify(
        _2b[0].countriesPercentageOfTotal,
        undefined,
        4
      )}`
    );
  } else {
    console.log(`2bc) No C02 emissions data found!`);
  }
}
