import { createInterface, ReadLine } from 'readline';
import { isUndefined } from 'util';
import { createReadStream } from 'fs';
/**
 * takes a line read from the .csv, splits it into an array of strings,
 * and then removes the additional quotation marks.
 *
 * @export
 * @param {string} readLine
 * @returns {string[]}
 */
export function parseCSVLine(readLine: string): string[] {
  return readLine
    .split(',') // it's a .csv...
    .map((value: string) => value.replace(/"+/g, '')); // remove the extra quotation marks
}

export function processFileLineByLine<T>(
  startLine: number,
  fileName: string,
  processLine: (line: string) => T
): Promise<T[]> {
  return new Promise((resolve, reject) => {
    const entireData: T[] = [];
    let lineNumber: number = 0;

    // could throw exception, will be caught in upper level...
    const fileStream = createReadStream(fileName);

    const rl: ReadLine = createInterface({
      input: fileStream,
      crlfDelay: Infinity,
    });
    rl.on('close', () => {
      return resolve(entireData);
    });
    rl.on('line', (line: string) => {
      // skipping the first 4 lines as they are irrelevent data
      if (lineNumber >= startLine) {
        entireData.push(processLine(line));
      }
      lineNumber++;
    });
  });
}

/**
 * this function will return a number rounded to two dp where necessary
 * the 0.00001 is to get around javascript weirdness when it comes to floating point numbers
 *
 * @export
 * @param {number} number
 * @returns {number}
 */
export function roundToTwoDp(number: number): number {
  return Math.round((number + 0.00001) * 100) / 100;
}

export function calcuatePercentage(total: number, value: number): number {
  return (value / total) * 100;
}

export function calculatePercentageTo2DP(total: number, value: number): number {
  return roundToTwoDp(calcuatePercentage(total, value));
}

export interface IAnswerFunction {
  answer: string;
  functionForAnswer: () => void;
}
/**
 * quick and dirty function that builds a CLI for demonstarting my code...
 * any errors are to be caught in the calling function
 *
 *
 * @export
 * @param {string} question
 * @param {string} failedToFindAnswerFunctionResponse
 * @param {...IAnswerFunction[]} answerFunctions
 * @returns
 */
export function cliBuilder(
  question: string,
  failedToFindAnswerFunctionResponse: string,
  ...answerFunctions: IAnswerFunction[]
) {
  const rl = createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => {
    function recursiveQuestion() {
      return rl.question(`${question}\n`, (answer: string) => {
        const formattedAnswer: string = answer.toUpperCase().trim();
        if (formattedAnswer === 'Q') {
          console.log('\n\nExiting...');
          return resolve(process.exit());
        }
        const foundAnswerFunction: IAnswerFunction = answerFunctions.find(
          (af: IAnswerFunction) =>
            af.answer.toUpperCase().trim() === formattedAnswer
        );
        isUndefined(foundAnswerFunction)
          ? console.log(`${failedToFindAnswerFunctionResponse}\n`)
          : foundAnswerFunction.functionForAnswer();
        return recursiveQuestion();
      });
    }
    return recursiveQuestion();
  });
}
