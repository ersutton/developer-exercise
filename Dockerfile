# Dockerfile for developer excercise :)

FROM node:8-alpine
WORKDIR /usr/app
COPY dist /usr/app/src
# commenting out the below line as we have no dependencies :)
# COPY node_modules /usr/app/node_modules
# Run the app
CMD ["node", "./src/index.js"]
